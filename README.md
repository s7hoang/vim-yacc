This is an indent file for yacc files in vim.

This indent file is my vim-lex indent file with some modifications to be
more compatible with yacc specific things.

There technically already is one that is included with the base installtion of
vim but, I found that it doesn't tend to indent everything the way I wanted it.
So, I wrote my own.
